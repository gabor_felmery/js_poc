// app.js
const express = require('express');
const AWS = require('aws-sdk');
const cors = require('cors');
const app = express();
AWS.config.dynamodb = { endpoint: 'http://localhost:3001', region: 'eu-west-1' };
const dynamoDbClient = new AWS.DynamoDB.DocumentClient();

app.use(cors({ origin: true, credentials: true }));
app.use(express.json({ extended: false }));

app.get('/', (req, res) => res.send('Hello world!'));
app.get('/hello-dynamo', (req, res) => {
    dynamoDbClient.get({TableName: 'table', Key: 'key'})
.promise()
    .then(result => res.send(result))
    .catch(error => res.status(500).json(error))
})

const port = process.env.PORT || 8082;

app.listen(port, () => console.log(`Server running on port ${port}`));