#!/bin/sh
npx mocki run --path backend/dynamo-mock.yml &
P1=$!
npm start --prefix backend &
P2=$!
npm start --prefix frontend &
P3=$!
wait $P1 $P2 $P3