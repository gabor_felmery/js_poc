import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import 'App.css';

import HelloWorld from './HelloWorld';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path='/' component={HelloWorld} />
        </div>
      </Router>
    );
  }
}

export default App;