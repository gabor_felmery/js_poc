import React, { Component } from 'react';
import 'App.css';
import axios from 'axios';
import { Link } from 'react-router-dom';

class HelloWorld extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ""
    };
  }

  componentDidMount() {
    axios
      .get('http://localhost:8082/hello-dynamo')
      .then(res => {
          console.log(res)
        this.setState({
          message: res.data.Item.Value
        })
      })
      .catch(err =>{
        console.log('Error from helloDynamo');
      })
  };


  render() {
    const message = this.state.message;
    console.log("Message: " + message);

    return (
        <div className="message">
             <h1>{message}</h1>
        </div>
       
    );
  }
}

export default HelloWorld;